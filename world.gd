extends Node2D

@onready var width = get_viewport().size.x
@onready var height = get_viewport().size.y
@onready var coin_sound = $CoinSound

var coin_coordinates: Vector2i
var scissors_coordinates: Vector2i
var power_up_coordinates: Vector2i
var coin = preload("res://coin.tscn")
var scissors = preload("res://scissors.tscn")
var bomb = preload("res://bomb.tscn")
var power_up_instance
var black_box: Array[Vector2i]

func _ready() -> void:
	pop_coin()
	$Snake.eaten.connect(_on_snake_eaten)
	black_box = []
	$BlackBoxTimer.start()
	$PowerUpStart.start()
	

func _process(_delta: float) -> void:
	if Input.is_action_just_released("quit"):
		get_tree().quit()
	if Input.is_action_just_pressed("restart"):
		get_tree().reload_current_scene()
		
func _draw():
	for box in black_box:
		draw_rect(Rect2i(box.x, box.y, Globals.STEP, Globals.STEP), Color.BLACK, true)
	
	
	
func pop_power_up() -> void:
	power_up_coordinates = get_random_coordinates()
	check_collision_with_black_boxes(power_up_coordinates)
	if randi() % 2 == 0:
		power_up_instance = scissors.instantiate()
	else:
		power_up_instance = bomb.instantiate()
	power_up_instance.position = power_up_coordinates + Globals.diag
	add_child(power_up_instance)
	
		
func destroy_power_up():
	$PowerUpStart.start()
	power_up_coordinates=Vector2i(-1,-1)
	if power_up_instance != null:
		power_up_instance.queue_free()
	

func pop_coin() -> void:
	coin_coordinates = get_random_coordinates()
	check_collision_with_black_boxes(coin_coordinates)
	var instance = coin.instantiate()
	var pitch_change: float = 1
	var scalation: float = .5 
	var points: int = 2
	var r: int = randi() % 3
	match r:
		1: 
			pitch_change = 1.5
			scalation = .25
			points = 1
		2:
			pitch_change = 0.8
			scalation = 1
			points = 5
	instance.pitch_change = pitch_change
	instance.scalation = scalation
	instance.points = points
	instance.position = coin_coordinates + Globals.diag
	add_child(instance)
	$Coin/AnimatedSprite2D.play("default")

func get_random_coordinates() -> Vector2i:
	var x: int = (randi() % (width/Globals.STEP))*Globals.STEP
	var y: int = (randi() % (height/Globals.STEP-1))*Globals.STEP + Globals.STEP
	return Vector2i(x,y)

	
func check_collision_with_black_boxes(cc: Vector2i):
	for box in black_box:
		if box == cc:
			print('collision')
			black_box.erase(box)
			queue_redraw()
		
	
func _on_snake_eaten():
	var node = $Coin
	coin_sound.pitch_scale = node.pitch_change
	coin_sound.play()
	node.queue_free()
	
	await get_tree().create_timer(0.5).timeout
	pop_coin()


func _on_black_box_timer_timeout() -> void:
	var box: Vector2i = $Snake.blocks[-1]
	if box.x % Globals.STEP == 0 or box.y % Globals.STEP == 0:
		box = $Snake.blocks[-2]
	black_box.append(box-Globals.diag)
	queue_redraw()


func _on_snake_dead() -> void:
	$BlackBoxTimer.stop()
	$PowerUpDuration.stop()
	$PowerUpStart.stop()
	if get_node_or_null("Coin"):
		#$Coin/AnimatedSprite2D.stop()
		$Coin.queue_free()


func _on_power_up_start_timeout() -> void:
	pop_power_up()
	$PowerUpDuration.start()


func _on_power_up_duration_timeout() -> void:
	destroy_power_up()


func _on_snake_power_up_eaten() -> void:
	destroy_power_up()
