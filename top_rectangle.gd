extends Node2D

const COLOR_TOP = Color.BLACK

func _draw():
	var width = get_viewport().size.x
	draw_rect(Rect2i(0, 0, width, Globals.STEP), COLOR_TOP)
