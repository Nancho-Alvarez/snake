extends Node2D

signal eaten()
signal power_up_eaten()
signal dead()

const diag := Vector2i(1,1)*Globals.HALF_STEP
var direction:=Vector2i.RIGHT
var new_direction:=Vector2i.ZERO
@onready var width = get_viewport().size.x
@onready var height = get_viewport().size.y
var i_am_alive: bool = true
var step: int = Globals.STEP
var half_step: int = Globals.HALF_STEP
var snake_position: Vector2i
var blocks: Array[Vector2i]
var body_color_normal := Color.hex(0x478cbfff)
var body_color_feroz := Color.hex(0xff0000ff)
var body_color := body_color_normal
var increase: int = 0
var feroz_state := false

func _ready():
	var start = Globals.start_position
	snake_position = (start + Vector2i.DOWN) * step + diag
	blocks = [snake_position,
			snake_position+Vector2i.LEFT*half_step*1,
			snake_position+Vector2i.LEFT*half_step*2]

	
func _input(_event):
	if not i_am_alive:
		return
	if Input.is_action_just_pressed("ui_left") and direction != Vector2i.RIGHT and new_direction != Vector2i.ZERO:
		new_direction=Vector2i.LEFT
		$Head.rotation = deg_to_rad(90)
	if Input.is_action_just_pressed("ui_right") and direction != Vector2i.LEFT:
		new_direction=Vector2i.RIGHT
		$Head.rotation = deg_to_rad(-90)
	if Input.is_action_just_pressed("ui_up") and direction != Vector2i.DOWN:
		new_direction=Vector2i.UP
		$Head.rotation = deg_to_rad(180)
	if Input.is_action_just_pressed("ui_down") and direction != Vector2i.UP:
		new_direction=Vector2i.DOWN
		$Head.rotation = deg_to_rad(0)

func _process(_delta):
	var p = snake_position
	if p.x < half_step or p.x > width - half_step or p.y < step + half_step or p.y > height - half_step:
		i_am_alive = false
		dead.emit()
	
	

func _draw():
	draw_polyline(blocks, body_color, half_step)
	draw_circle(blocks[blocks.size()-1], half_step/2, body_color)


func _on_timer_timeout() -> void:
	if not i_am_alive:
		return
	var p = snake_position as Vector2i
	
	# coin
	if p - diag == get_parent().coin_coordinates:
		increase = get_parent().get_node("Coin").points
		eaten.emit()
		
	# power_up
	if p - diag == get_parent().power_up_coordinates:
		var power_up = get_parent().power_up_instance
		power_up.effect(self)
		power_up_eaten.emit()

	# body blocks
	for i in range(1, len(blocks)-1):
		if p == blocks[i]:
			i_am_alive = false
			dead.emit()
			
	# black boxes
	var black_box=get_parent().black_box
	for box in black_box:
		if p == box+diag:
			if feroz_state:
				get_parent().black_box.erase(box)
				get_parent().queue_redraw()
				break
			else:
				i_am_alive = false
				dead.emit()

	
	if (p.x % step == half_step) and (p.y % step == half_step):
		direction = new_direction
	snake_position += (direction)* half_step
	$Head.position = (snake_position) as Vector2
	if snake_position != blocks[0]:
		blocks.insert(0, snake_position)
		blocks = blocks.slice(0,blocks.size()-1+sign(increase))
		increase = increase - sign(increase)
	queue_redraw()
	


func _on_feroz_timer_timeout() -> void:
	feroz_state = false
	body_color = body_color_normal
