extends Node2D

const SIZE = Globals.STEP - 2
@export var color1 = Color(1.0, 0.0, 0.0, 0.5)
@export var color2 = Color(1.0, 1.0, 0.0, 0.5)

var color: Color

func _draw():
	const STEP = Globals.STEP
	var width = get_viewport().size.x
	var height = get_viewport().size.y
	for x in range(0, width, 2*STEP):
		for y in range(STEP, height, 2*STEP):
			draw_rect(Rect2i(x+1, y, SIZE, SIZE), color1)
			draw_rect(Rect2i(x + STEP + 2, y, SIZE, SIZE), color2)
			draw_rect(Rect2i(x + 1, y + STEP, SIZE, SIZE), color2)
			draw_rect(Rect2i(x + STEP + 2, y + STEP + 2, SIZE, SIZE), color1)
			
